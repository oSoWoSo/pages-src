---
name: "Bug"
about: "File an issue for a bug"
title: "Fill out bug description here"
labels:
- bug
---
# Steps to reproduce the bug
<!-- Include OS, web browser, and actions taken -->

# What does this bug do?
<!-- Tell what the bug does, concisely. Does it break the website, hide something...? -->

***
Thank you for fixing this bug!