---
name: "Typo"
about: "File an issue for a typo"
title: "Fix a typo"
labels:
- typo
---
# Where is this typo?
<!-- Include page, sentence, and word. -->

***
Thank you for fixing this typo!