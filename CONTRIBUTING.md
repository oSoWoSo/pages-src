# Contributing
## First...
- Read the [Code of Conduct](./CODE_OF_CONDUCT.md).
- Look through issues.
# Report a typo/bug
Open a new issue if your issue does not exist, and fill out the appropriate template. Then submit. I will get to you soon after.
# Making changes
Make a PR with your changes. There is a high possibility I will reject.
# Forking for personal use
Please only fork the [layouts](https://codeberg.org/foreverxml/pages-templates) folder.
