---
title: "Contributions to FOSS by me"
---
A list of my contributions to major and minor FOSS projects alike.

- [GNOME 41.alpha: new icon for "Swell Foop" game](https://gitlab.gnome.org/GNOME/swell-foop/-/issues/20)
- [Nostalgia by tbernard: add wallpapers for GNOME 40 release](https://gitlab.gnome.org/bertob/nostalgia/-/merge_requests/13)

That's all for now, more will come later (hopefully).
