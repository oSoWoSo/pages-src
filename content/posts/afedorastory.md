---
title: "A Fedora story"
date: 2021-06-29T16:27:54-06:00
tags:
  - fedora
  - linux
  - gnome
  - windows
---
Once upon a time, I wanted an external hard drive, and spilled water on a computer. This _single_ hard drive would drive me to make a Hugo blog, join Mastodon, and so much more. Let me explain.
## Once upon a time...
...I spilled water on my computer. It no longer worked after that. We wrote "trash" on it and moved it to my basement. So for a computer, I took out my childhood computer. But it was too slow! So, I searched for solutions and found: Ubuntu! This was in late 2018, so the latest version was 18.10. I followed the online guide with Rufus, and I got Ubuntu installed! And I used that until I got my current computer, Christmas 2019. I liked it more so the Ubuntu just faded, faded--faded until it was gone. Then, June 2020. I rediscover the Linux. I use it on a trip to New Mexico, exploring KDE, XFCE, and the like. But I put it away too.
## And then...
My computer was running out of space and I needed a fix. I learned about "external HDDs" and decided to make one from an old computer. I took apart my computer, and got it. Mine had nothing on it, unlike my brother's. You see, my brother's old computer (just like mine) had photo backups on it, instead of Minecraft. Anyway, Christmas is here, and I get a shiny HDD enclosure. 5 days after, I look at the other computer. It says "trash" on it. 😨 **I accidentally took apart my brother's computer and wiped the hard drive!** Oh no... So then, I worked on reassembling it with parts from mine. I left out the display because it was too hard. But, you see, I forgot my Windows password.
## I liked Zorin OS!
I looked online for "best linux for windows user" and came across Zorin. I installed Zorin and  
it&nbsp;&nbsp;just&nbsp;&nbsp;worked.  
I also installed it on the OG Linux laptop and it worked well there too.
## The plot twist
I started reading about how customizing GNOME a lot creates bad RAM implications. And both the computers were running slow! So, I searched for unmodified GNOME and found it in *Fedora*. Fedora looked pretty and had the lastest GNOME 3.38, so I installed it on one. I customized it a bit and loved it! Then came the next one. Whoops, it runs bad. To Xubuntu. And to Fedora XFCE. And the headless dual-computer-merged-into-one? I loved it so much I used it more than my computer! So, I embarked on a task.
## The spread of Fedora
I shrunk my NTFS after deleting tons of stuff. Then got the Fedora USB, then installed. Nice! I customized it a bit, but didn't like it so reinstalled. And got GNOME 40. And 4 months later, here I am writing this in the Apostrophe app for GNOME, on GNOME 40, on my computer, on Fedora.
## Some more things
After Fedora, I searched for FOSS only. This lead me to Hugo, Mastodon, Markdown, adding wallpapers to a wallpaper app, and everything my computer has now.
## End
Now you know how a glass of water and a hard drive changed my primary OS and made me aware of Invidious. Thank you for reading this. See you soon!