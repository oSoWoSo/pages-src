---
title: "Backup day!"
date: 2021-05-04
tags:
  - backup
  - development
---
It may be an insignificant day, but today you should backup. Starting with making a new backup repository, and deleting the old one.
## Why?
This will very much help you keep size down. Anyway, let's go through the steps.
## Step 1: Enable Hidden Files
This can help you catch hidden folders that take up a ton of space.
## Step 2: Home Directory Scan
Delete files/folders you don't want, move files you want to a good location, and switch to the Downloads folder.
## Step 3: Downloads Sort
Move important files to their directories (Videos, Images, Documents...) and delete the rest.
## Step 4: Folders Sort
For each folder, delete the files you don't want, archive the ones you want but don't use often, and keep the ones you use often as is.
## _Optional:_ Step 5: Archive the Archives
For even larger amounts of storage saved, archive all of your archives in one archive. 
## Step 6: All your drives too!
Your flash drive, your backup drive, all of them.
## Step 7: Delete the Backup Folder, and Reconfigure the Backup
This will save you a ton of space.
## Step 8: Empty Trash
Obvious reasons.
## Step 9: Backup (_finally_)
And finally you backup. Wow! That was a lot.

I did this today and it made my life AMAZING. Also: Don't forget to run `sudo flatpak repair` to delete those unused refs. See you later!
