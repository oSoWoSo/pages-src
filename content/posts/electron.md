---
title: "Electron sucks, please do not use it."
date: 2021-05-06
tags:
  - electron
  - browser
  - bloat
  - development
---
Electron apps might be "easy to maintain" or "have nice features". But behind this, they just plain **_suck_**. Here's why.
## Disk size
Got a Chromium browser installed, like Opera, Vivaldi, Brave, or Chrome? Good, now check how large it is. 265 MB? That's big. Now imagine all of your apps were this size. That would quickly overload my drive! But Electron does just this. It bundles a Chromium engine with **every app using Electron, separately**, such as Telegram, RocketChat, Atom, Obsidian... the list goes on.
## RAM Consumption
Atom uses about 1 GB of memory to edit 1 medium-sized file. Now think about this: Most laptops have 8GB-16GB of memory. And usually you have some web browsers open while editing, with a lot of tabs (2GB), and some is taken by the OS (1-4GB). On a normal laptop, your laptop would run awful because you have 1GB RAM left. Now, if a long file was opened, or many opened... NO.
## Non-native
Electron apps are really just web pages. A web page can do everything an Electron app can do while being much smaller. Plus, there are things called PWAs that are basically Electron apps, but FAR better (smaller and work offline like Electron). All modern Chromium browsers support them. Heck, even GNOME Web (13.7 MB as an RPM) can install a website as a shortcut in Activities (applications), and even though this does not work offline it is still better than Electron.
## So, what to use instead?
Qt or GTK can serve you well. Qt works on every major platform and GTK does too. They are small, fast, and light, and do not take up much memory or processing power.

_That concludes this post!_ 
