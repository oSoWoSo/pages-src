---
title: "A fun recipe I made today: Grilled Cheese!"
date: 2021-07-08T12:40:40-06:00
tags:
  - recipe
  - food
  - easy
---
*Est. time: 2-3 minutes total*
## What you Need
- 2 slices of toast
- Shredded cheese
- A toaster
- A microwave
- *Optional:* Seasoning
## How to Make
1) Toast the bread on the "Bread" and "3.5" settings.
2) As soon as it comes out, put one slice on a plate.
	1) Put cheese on top, and put the seasoning on top.
	2) Then put the other slice on top.
3) Microwave for 30 seconds.
4) Cut and eat with a drink.

I had this for lunch and it is good. Also really fast to make.
