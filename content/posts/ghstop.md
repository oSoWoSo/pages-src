---
title: "STOP using GitHub!"
date: 2021-06-30T10:40:21-06:00
tags:
  - github
  - foss
  - linux
  - rant
---
You might know [GitHub Copilot](https://copilot.github.com/) (proprietary), a tool in VS Code (proprietary) to help write code (open source) to publish to Github (proprietary). That's usually _open-source_. Now, look at this toot on Mastodon (link [here](https://mastodon.social/@fribbledom/106500319103925079)):
> "Once, GitHub Copilot suggested starting an empty file with something it had even seen more than a whopping 700,000 different times during training -- that was the GNU General Public License."

This is REALLY bad. Proprietary tool violating GPL's left and right. Also, have you ever heard [of the 3 E's](https://en.wikipedia.org/wiki/Embrace,_extend,_and_extinguish)? Github is seeking to do this with FOSS and Microsoft wants to be supreme over the Linux community. So, if your FOSS project is on GitHub, it is somewhat FOSS. It could be deleted at any time. So please, I beg you, move to Codeberg, or at least Gitlab. Thank you for reading, and please tell your friends to jump the Github ship too, and do so yourself too. 
