---
title: "Incognito mode: What is it?"
date: 2021-07-18T21:50:55-06:00
tags:
  - web
  - tech
  - incognito
---
Incognito mode on [Chrome](https://www.google.com/chrome/), otherwise called [InPrivate](https://www.microsoft.com/en-us/edge), [Private Browsing](https://www.apple.com/safari/), [Private Window](https://www.mozilla.org/en-US/firefox/new/), or [Secret mode](https://www.samsung.com/us/support/owners/app/samsung-internet), among others like-named, are a way of hiding activity. They all do the general same thing: delete browsing data on exit. I will explain pros and cons of Incognito, which i will call Private for now.
## Pros
### Private mode hides your activity from other users of the computer
If you are using someone else's computer to check emails, look at documents, etc. use a private window. Once you close it all the data is *poof, gone*! Never existed.
### Private windows are seperate
Two private windows will not share data (in my experience with GNOME Web/Safari). That means you can have work, personal, and school in all different private windows. But I doubt you would need that right now...
### They can resist some tracking
Since these things called cookies, history, and local storage are erased after closing the window, it will resist some advertising. Some newer browsers also turn off third-party cookies, commonly used to track you, in private windows.
## Cons
### Biggest one: PRIVATE MODE DOES NOT PROTECT YOU FROM TRACKING!
It does not! Your IP address can be used for tracking linking your private window back to you. Scary!
### Other people CAN see your activity
Look at the heading of the first pro. It says it hides activity from the users of the computer only. It does not hide activity from your ISP (internet service provider) and others. As stated on the GNOME Web private window new tab page, 
> **Incognito mode hides your activity only from people using this computer.** It will not hide your activity from your employer if you are at work. Your internet service provider, your government, other governments, the websites that you visit, and advertisers on these websites may still be tracking you.

This is all true. You need a full tracker blocker like the ITP in Safari/GNOME Web and uBlock Origin to stop tracking.
### Not annonymous
Because of the above, private windows are not really annonymous. I will explain a solution soon.
## So it just keeps going... Is there a solution?
Yes! [Tor Browser](https://www.torproject.org/) can help you. It has a system that makes it so what you are doing can not be traced back to you, through onion-routing. And your work, ISP, governments, etc. cannot see what you are doing.
### But Tor is for dark web and criminals!
Less than 3% of all Tor sessions visit `.onion` sites where the dark web is. And most of them are to the [DuckDuckGo](https://duckduckgo.com/) onion site. This logic is similar to saying "bank robbers use highways so we should not use highways!"
## Another solution, please...
Install [uBlock Origin](https://ublockorigin.com/) on your browser! Or if you cannot, enable the built-in tracking prevention in your browser, if available (turn this off with uBlock, as uBlock works worse with it).

Thanks for reading!