---
title: "Why should you install Linux?"
date: 2021-06-28T12:11:53-06:00
tags:
  - linux
  - windows
---
Yes, you. Right there. Using Windows? Yes? You might not want to. There's [this](https://www.digitalcitizen.life/top-11-things-we-dont-about-windows-10/), and [this](https://qr.ae/pGntTU), and [this](https://www.maketecheasier.com/things-suck-about-windows10/), and [this](https://www.reddit.com/r/Windows10/comments/7vsng0/why_does_windows_10_suck_soooo_badly/)... Why the FRICK does this even have to be a problem? Everyone complains about Winblows and yet Microsoft does **nothing**. So, the solution is simple: _stop hecking using Windows._ And most people would say, "macs are too expensive!!". But, what if I told you there are more options? There's Fedora, openSUSE, Ubuntu, elementary... many distributions of Linux, as it is called. I will tell you why you should make the leap of faith as so many others have.
## Listed above
All the five links listed above. All of them show **different things** wrong with Windows. And yes, macOS is free, but:

1. You need a Mac to run macOS.
	- Macs are usually _very_ expensive.
2. A lot of software on Macs is paid.
3. macOS is proprietary (not free as in freedom).

Really, the only fix to the above 3 problems is Linux. So, next reason.
## It is free (free beer, and freedom)
Linux is cheaper than a yogurt. It is 0$, 0£, 0€... The list could go on! There's no price. Also, it's freer than the US of A (USA) (US). You can "fork" any Linux and make your own version of it (MATE and LibreOffice did this). You can also help make a Linux, from your home! (I do this.)
## Runs better
Know how Windows consumes half of your RAM in idle? Not Linux. Linux ranges from 1GB to 10MB of RAM used. Much better! 
## And Windows 11...
Look at the posts before this one on the #windows tag. You'll see the whole fiasco.
## So...
...get a Linux Mint/Fedora ISO, flash it, and install. You're on a better path already.

