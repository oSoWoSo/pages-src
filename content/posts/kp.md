---
title: "A guide to KeePass and how to get started/use it"
date: 2021-07-14T20:54:34-06:00
tags:
  - keepass
  - development
  - password
---
So, you want a password manager. KeePass can help you control your data; and gives you control. Here's how to start/use it.
# Starting
## • Get a client
A KeePass app (client) is what you need to use KeePass. You can universally use [KeeWeb] or [KeePassXC] on any desktop, use [KeePassDX] on Android, and [Keepassium] on iOS. (If you use Linux/GNOME, use [Password Safe].)
## • Make and remember ONE secure password
This might sound counterintuitive, but you only need to remember one password. It will give you access to your KeePass and you can get all passwords from there.
## • Import your passwords
In *KeePassXC*, what I will use for this tutorial, you can import passwords from CSV, 1Pass, or KP1. Make sure to export from your existing password manager, which is most likely your browser. It should be a CSV. Then, import with KeePassXC and choose what columns should be what to avoid mess-ups. Now, delete those browser passwords! Anyone can access them. It's really not a good idea to use the browser password manager.
### ‣ If you don't have your passwords in a password manager...
...no trouble! Make a new KeePass safe and write all your passwords down in it.
## • Store EVERYTHING in it
Store your library card, WiFi password, and other items like this in your KeePass. KeePass is *encrypted*, so nobody can access anything unless they know your KeePass password.
## • Back it up EVERYWHERE
If you have a phone, an external hard drive, a Dropbox, a Nextcloud Drive, and a Google Drive, copy it over to your phone, external HDD, Nextcloud, Dropbox, and Google Drive. You don't want to lose all your passwords!
## • Look at Database Reports
In Database > Database Reports... in KPXC, you can look at weak/reused passwords and change them. You do not need to remember these passwords; only your KeePass password.
## • On your way to greatness!
You're now much better at security than an ordinary person. Keep doing a great job!

[KeeWeb]: https://keeweb.info/
[KeePassXC]: https://keepassxc.org/
[KeePassDX]: https://www.keepassdx.com/
[Keepassium]: https://keepassium.com/
[Password Safe]: https://gitlab.gnome.org/World/PasswordSafe

