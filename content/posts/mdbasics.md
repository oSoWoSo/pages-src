---
title: "Markdown: The basics"
date: 2021-06-27T15:58:11-06:00
tags:
  - markdown
  - development
---
Markdown is what I use to write this website (and Hugo generates it, but that is a different story). It allows me to quickly write, with *no* distractions. So what is Markdown? How do I use it? I will tell you.
## What in the world is Markdown?
It is a way of writing that can be interpreted by a machine to make *articles* and **blog posts**.It allows text formatting without your hands leaving your keyboard.
### But how?
It is a *way* of writing. Not something like Word. For example, surround a word with \* and it becomes italic. \*Hello\* becomes *Hello*. And \\\* becomes \*. And more...

Markdown is easy. But why is it designed this way?
## WYSIWYG editors
WYSIWYG editors, otherwise known as *what you see is what you get editors*, are like **Microsoft Word** and the such. You have formatting buttons to turn on *italic*, **bold**, and ***both***. In Markdown, these are characters, like the asterisk I showed before. (_I will leave links to learning Markdown at the end._)  
> So, what you *see* while you make it is **NOT** what you *get* at the end.  

But it has the upside of being _tiny_. What you read to here would be about 1.05 kB in Markdown. So, it's small.
### What about LaTeX?
LaTeX is *really* powerful, but it's hard to use. Markdown is extremely easy to use. To make a title, you have to do
```tex
\title{Title here}
```
 in LaTeX, while in Markdown you just do this:
```md
# Title here
```
 So Markdown makes life **easy**! But, how do I learn it? You can learn [from my friend's website](https://zxo.codeberg.page/posts/markdowntutorial/) and some advanced stuff [from one of my posts](https://foreverxml.codeberg.page/posts/mdtut/). Have a great day!