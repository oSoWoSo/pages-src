---
title: "Improving focus with Markdown and LaTeX"
date: 2021-06-28T13:27:56-06:00
tags:
  - latex
  - markdown
  - development
---
Markdown and LaTeX (styled \LaTeX in LaTeX) are not traditional text editors. They are *ways* of editing text to make it easier to make articles, documents, slideshows, and much more. I will explain Markdown and LaTeX and if you should use them or not.
## What is Markdown?
Markdown is a so-called *"language"* that you write in.
It includes support for *italic*, **bold**, and ***both***, as well as *m***a*n*y** other things.
Like quotes:
> _You can't trust quotes you see on the Internet._ - Abraham Lincoln

And code:
```
Markdown is a so-called *"language"* that you write in.
It includes support for *italic*, **bold**, and ***both***, as well as *m***a*n*y** other things.
Like quotes:
> _You can't trust quotes you see on the Internet._ - Abraham Lincoln 
```
And lots of other things.
***
Now, Markdown does not have an *interface*. That "code block" you saw before was the quote, and the text before the quote.
## So what is LaTeX then?
LaTeX is like Markdown, which is not a [WYSIWYG](https://en.wikipedia.org/wiki/WYSIWYG) editor. LaTeX is really powerful, but hard to use for a beginner. I recommend writing in Markdown (what this was originally writter in), and converting to TeX and then to PDF or converting to HTML for a post. Once you convert to TeX from Markdown, you can add special LaTeX things like the formatting of \LaTeX in LaTeX.
## And should I use it?
If you need to really care about formatting (e.g. where images are placed) use a WYSIWYG editor. But to simply write without distractions, use Markdown. And then convert to LaTeX and PDF and DOCx and HTML and everything. And if you want, make some changes there! And make sure to read my [Markdown Basics](https://foreverxml.codeberg.page/posts/mdbasics/ "Has links!") guide if you want Markdown.

_Note: This document was made with Markdown. Click [here](https://codeberg.org/foreverxml/pages-src/raw/branch/main/data/mdtex.pdf) if you want to se it in LaTeX/PDF._
