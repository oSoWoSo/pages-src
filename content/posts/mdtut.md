---
title: "Markdown tutorial"
date: 2021-06-26T14:58:39-06:00
tags:
  - markdown
  - development
---
Know the basics of Markdown before this!
## Advanced headers
Enter this:
```md
Abc
===
Def
---
```
Shows up as this:
***
Abc
===
Def
---
***
## Code
### Inline
Inline code is easy. Just do this:
```md
`foo: bar;`
```
Goes to:
***
`foo:bar;`
***
And there's some more.
### Code-block
You can make code blocks like this:
~~~md
Lorem ipsum
```
code block
enter preserved
``` 
dolor sir amet.
~~~
This renders to:
***
Lorem ipsum
```
code block
enter preserved
``` 
dolor sir amet.
***
And alternate code blocks:
```md
I have
~~~
a code block.
~~~
It is nice.
And I...

    am
    indented code!
```
It renders to:
***
I have
~~~
a code block.
~~~
It is nice.
And I...

    am
    indented code!
***
#### Syntax highlighting
It is easy, do as follows:
~~~md
```css
body {
	font-family: sans-serif;
	color: white;
}
```
```md
I am _markdown_.
```
~~~
Renders to:
***
```css
body {
	font-family: sans-serif;
	color: white;
}
```
```md
I am _markdown_.
```
***
Many options for code blocks!
## Links
Links are easy. This is how:
```md
[Example](https://example.com)  
[Link]  
[Linky][3]

[Link]: https://wikipedia.com
[3]: https://foreverxml.codeberg.page
```
Renders to:
***
[Example](https://example.com)  
[Link]  
[Linky][3]

[Link]: https://wikipedia.com
[3]: https://foreverxml.codeberg.page
***
### Images
Images are similar, but a SMALL twist. See:
```md
![Example](https://placebear.com/200/300)
![Image]
![Bear][7459]

[Image]: https://placebear.com/500/500
[7459]: https://placebear.com/300/600
```
Renders to:
***
![Example](https://placebear.com/200/300)
![Image]
![Bear][7459]

[Image]: https://placebear.com/500/500
[7459]: https://placebear.com/300/600
***
And, finally...
## Horizontal Lines
Super easy. Just:
```md
***
text
___
lines 

---
```
Becomes:
***
***
text
___
lines 

---
***
Interesting.

But that's all, see you _soon_...

_(Note: This is not a complete post, some was removed from the original post.)_