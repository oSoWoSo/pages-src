---
title: "Windows and Microsoft are jokes"
date: 2021-05-07
tags:
  - microsoft
  - development
  - joke
---
Windows says they are "focusing on design" next release. But look at this:

![old vs new icons](https://cdn.vox-cdn.com/thumbor/TZhBibgMNoWAVsEWO8YrxVMgnVI=/0x0:885x436/920x0/filters:focal(0x0:885x436):format(webp):no_upscale()/cdn.vox-cdn.com/uploads/chorus_asset/file/22495516/Windows_Shell32_icons.jpg)

How is _this_ **any** better??? There are **still** mnay things wrong with this. The window title icon is the same, a lot of these are not updated (window icon!!) And, look at this mess:

![30 different context menus, by Windows](https://i.redd.it/xvngsdy1yqd61.png)

Oh my fricking god. THIRTY **DIFFERENT** FRICKING CONTEXT MENUS. What type of JOKE is this? A bad joke? Yes, kind of like the fricking keylogger built in to Windows! Oh, RAM usage too. And disk usage. And more telemetry... ugh. Just use Linux and you don't have this whole joke OS with joke programs.

