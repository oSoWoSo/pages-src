---
title: "An introduction to Linux"
date: 2021-06-23T22:28:35-06:00
tags:
  - linux
  - kde
  - gnome
  - fedora
  - development
---

_This is explained based on Windows, not macOS. Keep that in mind._

You may have heard about a "Linux" operating system to combat the Windows. What exactly is it? I will cover that in this blog post.
## Basically...
...Linux is an operating system, just like your Windows. An operating system is what handles you moving your mouse, clicking on one of those icons on your background picture or that funky bar at the bottom, and allows you to type in a Word document. But the so-called "Linux" is very different from the Windows you know. Here's why.
## You have some choice!
You know how you can choose between Internet Explorer and Chrome? You can do the same on Linux, except with nearly everything. You can change the UI as well as your base OS (operating system). A UI stands for _user interface_, and it's what Windows/Linux displays for you. Your Windows, icons on the background picture (otherwise called "wallpaper" or "desktop") and the Windows/Start button. Anyway, a _Linux_ UI can range from a Windows-like UI to a completely abstract UI. These UI variations are called "desktop environments", and here's what they are.
### KDE Plasma
The name _KDE_ doesn't mean anything in general. What you need to know is the Plasma (KDE Plasma) is like your Windows 7 UI, if you've ever used Windows 7. You know how Windows 10 is slow, while Windows 7 was fast? Plasma is like good old Windows 7. It is fast and nice. Here are some pictures:

![plasma 5.21 dark](https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2F149366088.v2.pressablecdn.com%2Fwp-content%2Fuploads%2F2021%2F01%2Fplasma-5.21-768x432.jpg&f=1&nofb=1)

![plasma 5.21 system monitor](https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fi0.wp.com%2F9to5linux.com%2Fwp-content%2Fuploads%2F2021%2F02%2Fkdeplasma-2.jpg%3Ffit%3D1920%252C945%26ssl%3D1&f=1&nofb=1)

![kubuntu 21.04](https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fubunlog.com%2Fwp-content%2Fuploads%2F2021%2F04%2FKubuntu-21.04.png&f=1&nofb=1)

![fedora 34 with plasma 5.21](https://spins.fedoraproject.org/static/images/screenshots/screenshot-kde.jpg)

As you can see, it looks like Windows and many people love it because of this. But there are other options too!
### Cinnamon
Cinnamon is another _Desktop Environment_. It functions similar to KDE, and is similar but different. Here's what it looks like:

![linux mint ulyssa](https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fwww.getmyos.com%2Fapp_public%2Ffiles%2Ft%2F1%2F2021%2F01%2Flinux-mint-20-1-january-2020-s2.png&f=1&nofb=1)

![fedora 34](https://spins.fedoraproject.org/static/images/screenshots/screenshot-cinnamon.jpg)

![ubuntu cinnamon](https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fsempreupdate.com.br%2Fwp-content%2Fuploads%2F2020%2F03%2FGestor-de-archivos.jpeg&f=1&nofb=1)

Windows users like Cinnamon because it behaves nearly **exactly** like the Windows. Here, I can show you:

![linuxfx](https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2F1.bp.blogspot.com%2F-GxAQvO-5OZw%2FX2o7yOFVmmI%2FAAAAAAAARUw%2FSuappwhHaasnmLXQgwNJe53x1zpqRw86QCLcBGAsYHQ%2Fs1912%2Flinuxfx-4-file%252Bmgr.png&f=1&nofb=1)

It's hard to believe that this is not Windows, and is actually the Cinnamon "DE".
### GNOME
Things vary here. GNOME looks and feels **very** different from Windows. Here, see for yourself:

![gnome](https://www.gnome.org/wp-content/uploads/2021/03/overview.png)

![gnome](https://www.gnome.org/wp-content/uploads/2021/03/notifications-1024x576.png)

![gnome weather](https://help.gnome.org/misc/release-notes/40.0/figures/weather.png)

![gnome](https://help.gnome.org/misc/release-notes/40.0/figures/shell-overview.png)

As you can see, GNOME is not at all like Windows. Many people like it, though, and it is default for a lot of Linuxes. It can be changed, as shown here:

![ubuntu](https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fwww.debugpoint.com%2Fblog%2Fwp-content%2Fuploads%2F2020%2F11%2FUbuntu-21.04-Hirsute-Hippo-Dev-Build.jpg&f=1&nofb=1)

Even though it is different, different things can be good if you want to learn them! I use GNOME and very much like it.
## And more things.
You have choice of operating system (Ubuntu or Fedora), choice of apps, choice of everything... These are not important, except for the commands run in the Terminal. But what even IS the terminal? I'll explain.
## Terminal
The terminal is an app where you type stuff and things happen. For example, you can type `sudo dnf update` and update all of your apps. Or, type `sudo pacman -Syu chrome` and update all of your apps _while_ installing Chrome. You can `touch hello.txt` and make a file called `hello.txt`. The possibilities are endless! But you can mess up some things as well. Running the command `sudo rm -rf /` is like dragging "My Computer" to the Recycle Bin. Not good! You used to have to do everything on this "Terminal", but no longer! You can be free from the terminal. You have GNOME Software and Plasma Discover to install apps and update apps, there's a nice tour in GNOME... in a nutshell, you rarely ever need to use the Terminal.
## Some complications
### You can't natively run a .exe file
Know the `.exe` or `.msi` you use to install stuff on Windows? Linux is unable to use these. Linux has to use `.tgz`, `.tar.gz`, `.deb`, and `.rpm`.
### Hardware issues
Sometimes WiFi, Bluetooth, and internal sound may not work. This is because Windows has better hardware support. Hardware is everything that your computer phisically has, such as a display, keyboard, or speaker.
## And how do I install this "Linux"?
You will need a USB drive and internet connection. To install, you can read a guide from @zxo@mastodon.social right [here](https://write.as/zxo/the-3-methods-to-install-linux-with-a-liveusb) about methods of installing Linux. You should also pick a Linux OS, such as [Fedora](https://spins.fedoraproject.org/), [elementary OS](https://elementary.io/), [Zorin OS](https://zorinos.com/), or [Ubuntu](https://ubuntu.com/download/flavours). _Note: to install GNOME, go to these websites for [Fedora](https://getfedora.org/en/workstation/download/) and [Ubuntu](https://ubuntu.com/download/desktop)_.

And, that's all for today. Happy day!

