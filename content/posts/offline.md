---
title: "Offline my blog!"
date: 2021-07-01T19:11:29-06:00
tags:
  - offline
  - blog
---
So, you want to view my blog offline. Here's how you can do that.
# Getting it ready
## Setting up the tools
Set up the tools! It's pretty easy.
### Windows
Windows is special. You need to do a couple things.
#### Chocolatey
Chocolatey is a package manager for Windows. It allows you to install `git` and `hugo` easily. Here's how to set it up:
***
First, launch an **admin** PowerShell. Admin is important, as it is the only way to install Chocolatey. Now, run this line of code:
```powershell
Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
```
Then, type `choco` once it is done. Does it provide an output? Good! Now let's continue.
#### Hugo
In the same **admin** PowerShell, copy in this command and run it:
```powershell
choco install hugo-extended -confirm
```
It will install the version of Hugo that can be used to compile my site.
#### Git
In the **admin** PowerShell again, run this command:
```powershell
choco install git -confirm
```
Now it's all done!
#### All the commands in one
```powershell
Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1')); choco install hugo-extended -confirm; choco install git -confirm
```
That will do everything at once.
### macOS and Linux Method 1
#### Homebrew
Homebrew is also a package manager. It's pretty easy to set up, this is how (inside the default macOS Terminal app, or your favorite Linux Terminal app):
```bash
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
```
#### Hugo
Using Homebrew:
```bash
brew install hugo
```
#### Git
Using Homebrew:
```bash
brew install git
```
#### All the commands in one
```bash
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)" && brew install hugo && brew install git
```
That will do everything at once.
### Linux method 2
Choose the command that matches your system for each. Else, use the method 1 or do some advanced stuff and install them natively.
#### Hugo and Git
```bash
sudo apt-get install hugo git # Debian/Mint/Ubuntu
sudo dnf install hugo git # Fedora
sudo pacman -Syu hugo git # Arch
```
## Fetch for the first time
This is really simple. First, navigate to the folder you want to clone it to, and then open a PowerShell/Terminal there. Then, run the following command:
```bash
git clone https://codeberg.org/foreverxml/pages-src.git foreverxml-blog
```
## Recurring fetch
Also extremely simple. Open PowerShell/Terminal in the `foreverxml-blog` folder and run the following command:
```bash
git pull
```
That's all! It's updated.
# Actually, you know, viewing it
Open PowerShell/Terminal in the `foreverxml-blog` folder and run the following command:
```bash
hugo server
```
Then, open the `localhost` link the PowerShell/terminal fives you in the browser, and voilà! You're done. Close the terminal to stop viewing it.

