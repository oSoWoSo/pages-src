---
title: "Why I make posts on this blog (personal)"
date: 2021-06-26T22:05:22-06:00
tags:
  - blog
  - development
  - personal
---
You may notice that I churn out posts on this blog. I'd like to explain why I make 3-4 posts a _day_ instead of doing other things.
## 1. Most of my life right now is this blog, Fedora/GNOME, and Mastodon.
I don't have much else happening in my life. I may go on a couple camping trips and hike, but my life at home in my house is just of that above. Really, it's *all*! Because of this I suck at interacting with people, etc.
### For example...
...today my mom took me to a restaurant (which was GREAT!!!!) and now I am tired. Oh how I want to cuddle my blåhaj!

So it it a bit stressful for me to interact with people. And currently, I am not in school (summer where I live) and that makes me ~~lazy~~ itching to find stuff to do. This results in my many posts, me doing karate (I am a 2nd degree purple belt in it), and contributing to FOSS (contributions page of top bar). Now, next one.
## 2. Friends drift away and back...
...and this summer they're away. Last summer, too, but this summer is the same. Basically the only people I talk to are the people I live with and you guys (if that's even *classified* as talking). So this blog & my Mastodon are outlets for me to let my feelings out, that may, or may not, be ephemeral. Actually, it's quite invidious how so many people in the GenZ talk to their friends all the time but due to lack of resources other can't...
 Back on track we need to go.
## 3. ...but we're the ones who change most.
And I am a prime example of this. 5 months ago I would not have cared if you used proprietary software or not. 1 year ago my dream job was working for Samsung! And I was using SMS and Google Photos! But now, no cloud backups and Fedora on my computer. I've changed so much in this sense, and my friends haven't. One of my male friends (who I will not name) still uses Windows on a low-spec PC with smol storage. _He would be much better off using BTRFS with its compression_, but he does not like to fiddle around with computers. No site-generating Go libraries, I guess...
## End
I'm really sorry for making a personal post. You can find them all in #personal if you like them. But if you don't, just ignore these. Thank you for reading, I had to get this off my chest.