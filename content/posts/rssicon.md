---
title: "RSS Icon - how did I get it?"
date: 2021-06-30T14:17:11-06:00
tags:
  - development
  - rss
---
Footer RSS! It's really easy in the theme I use, you just add this to your `config.toml`:
```toml
[params.footer]
    trademark = true
    rss = true

    topText = [
      "Powered by <a href=\"http://gohugo.io\">Hugo</a>",
      "<a href=\"https://example.com\">Website source</a>"
    ]
```
Something else for site source, obviously. But the `rss = true` puts the RSS icon there. Happy day!