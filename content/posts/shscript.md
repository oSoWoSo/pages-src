---
title: "Shell scripts I use"
date: 2021-06-26T15:48:30-06:00
tags:
  - sh
  - shell
  - development
---
I use some shell scripts to help me get stuff done faster. Like this one, in the root page folder for my [Hugo](https://gohugo.io) site:
```bash
#!/bin/bash
hugo
cd public
git add -A
git add posts/*
git commit
git push
cd ..
git add -A
git add content/*
git commit
git push
```
It generates and pushes my website to its destination (where you are now) and the source.
## Theme mods
And modifications to my hello-friend theme:

At `page/layouts/posts.html`:
```html
{{ define "main" }}
{{.Content}}
<ul class="all-posts">
{{range .Site.RegularPages}}
<li><a href="{{.Permalink}}">{{.Title}}</a></li>
{{end}}
</ul>
{{ end }}
```
At `page/layouts/shortcodes/invidious.html`:
```html
<div class='embed-container' style="width:100%;position:relative;overflow:hidden;padding-top:56.25%">
 _ <iframe src='https://invidious.40two.app/embed/{{ index .Params 0 }}'
	 frameborder='0' allowfullscreen style="position:absolute;top:0;left:0;bottom:0;right:0;width:100%;height:100%;" />
  _</iframe>
</div>
```
At `theme/layouts/partials/social-icons.html`:
```html
{{ range . -}}
    &nbsp; <a href="{{ .url }}" target="_blank" rel="me" title="{{ .name | humanize }}">{{ partial "svg.html" . }}</a> &nbsp;
{{- end -}}
```
Only thing changed: `rel="me"`.

This is mostly for if I lose my computer.
That's all, see you soon!