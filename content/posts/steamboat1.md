---
title: "Steamboat Springs, and a Friday-Tuesday blog (part 1)"
date: 2021-07-03T17:05:32-06:00
tags:
  - fri-tues
  - steamboat
  - rss
---
Actually part 2, but using dev numbers. So, today I woke up and had a nice cup of coffee. Then packed my laptop and phone and some clothes. Why? Going to Steamboat Springs, Colorado! We pack in for a four-hour ride. And 5 hours later, we're there. I did some things along the way: new RSS feed acquired! But that's really all that today was. I will have dinner soon and am looking forward to it. See you soon, friends!