---
title: "Steamboat adventures (part 2)"
date: 2021-07-04T12:05:27-06:00
tags:
  - fri-tues
  - steamboat
---
We had a egg-cheese breakfast. Then we hang out, watch a parade, and HOT SPRINGS! It was really nice, considering I slept bad. 2 hours later, we come back home and I perform a wikiHow I learned by making a makeshift AC. It cooled us down. Then, we went to watch a rodeo with dinner as a joke, but there were serious people there. They liked the USA, lol. I dislike it. Not to mention, today was america day. And we came back home, and set up ventilation. And here I am now.
# Fan Hacks
## Air Freshener
### What you need
- Table/box fan
- Dryer sheet

OR

- Paper towel
- Air freshener or soap
### How to do
Turn on the fan, and put the dryer sheet or paper towel with air freshener on the back. Instant good smell air!
## Warm and cool
### What you need
- A ceiling fan
### How to do
When the fan is off, look at what attaches it to the ceiling. There should be a switch between "W" and "S". Depending on Winter or Summer, switch it to the correct letter.
## Crosswind
### What you need
- A space with 2 windows
	- Can be many rooms
	- Needs a direct path between the two rooms that wind can travel through
	- Need outlets near windows
- 2 box fans
### How to do
In the space, make sure to make a path and close all other doors on the path unless they have cross breeze too. Then, set one fan to blow air in on the windowsill in front of an open window and the other one blowing air out. Enjoy the crossbreeze!
### Optional
Turn on ceiling fans on "winter" to expel warm air.
## Air C
### What you need
- A table/box fan
- A bowl
	- A waterbottle or cup works fine too
- Water
	- Ice cubes work fine
- Salt
- A freezer
### How to do
In the bowl, combine water and 15mL (3tsp) of salt. Stir and freeze. After it is frozen, place it in front of the fan and make sure the air hits it at an angle. Sit close and enjoy the cool! You can reuse the water.
## Double Cool
### What you need
- Lots of all sorts of fans, for spreading air around apartment/house
- An AC
### How to do
Put on all the fans going to every room from the AC unit. Then turn the AC on. AC for all the house!