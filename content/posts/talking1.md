---
title: "I need to talk to you guys about some things. (personal, cw)"
date: 2021-06-27T22:46:13-06:00
tags:
  - personal
  - cw
  - bullying
  - suicide
  - drugs
---
##*CW: suicide, drugs*

I know this blog is usually about Linux, Markdown & such. But this blog post will have a drastically different dynamic.

## Post start
In light of the recent ~~suicide~~ mass murder committed by ~~one of the devs of one of the best SNES emulators~~ people at KiwiFarms through cyberbullying, I would like to talk about cyberbullying, support, and why it is good to support your friends & family.
## What happened?
[Near](https://near.sh), a developer, ~~commited suicide~~ got [remotely](https://docs.google.com/document/d/12pOhaaFh998B0kyc5Sm4IhlhIp1c9t5gDNTVVPaiJgI/edit) [murdered](https://www.nintendolife.com/news/2021/06/the_dev_behind_one_of_the_worlds_best_snes_emulators_has_taken_their_own_life) by KiwiFarms. Near was the developer of `bsnes`, the best SNES emulator. They were killed less than 48 hours before this post came out. This matters a lot, as nobody just "commits suicide". They take in their surroundings and sometimes they can't deal with it. They were LGBTQIA+, like me. I am also *LGB**T**QIA+*, but now this does not matter. LGBT suicide rates are higher than average. And this is due to bullying (I haven't really come out on the internet much, no no bullying for me. Beside the point) that Near received a lethal dose of. From 4chan, KiwiFarms especially, and anywhere. This isn't okay! People think cyberbullying does not impact. It does!!!! **IT FRICKING DOES!!!!** Cyberbullying makes an impact. When will you smooth nut-brains realize this? It can cause people to wipe themselves off the planet Because of YOU!!!! *YOU!!!!*
## How do we prevent this? (NOTE to parents of children)
By supporting our friends and children. If they have ASD like me, tell them it's fine! Albert Einstein had ASD! Are they LGBT+? Support them, and tell them the truth: Most people are supportive! Except the dumb ones. And if you are an individual being bullied, close your online accounts and enjoy life offline. Have tons of friends to support you. And if you need the internet, no social websites like GH, Facebook, even blog comments!, etc. And if you struggle with suicidal thoughts, get therapy or call the suicide hotline.  
Thank you for reading this article in its entirety.

