---
title: "Some things I like to have"
date: 2021-06-27T15:58:22-06:00
tags:
  - things
  - personal
---
I have some things that I have used daily for a long time. I'd like to tell them to you.
## 1. [DOHM sleep sound](https://yogasleep.com/collections/natural-sound-machines/products/dohmclassic)
I have used this *every day* since I was a little baby and it is the only thing that makes me sleep. USD$50 well spent :)
## 2. My Samsung Notebook 9 Pro (laptop)
It has reasonable drivers on Fedora. Very good, and Type-C charging.

I believe that's all??
Even so this post isn't useless- I can tell you all about the sleep sound I use. That's all, bye.

