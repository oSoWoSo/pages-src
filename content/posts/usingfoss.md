---
title: "Using Free, Open SOurce Software"
date: 2021-05-04
tags:
  - development
  - foss
---
It has to do with a lot of things. I like to see what my software does and be able to contribute to it. Notice how I said "mostly": I still need some proprietary software for my school.
## Table of Contents
- Proprietary List
	- Why do I use these?
- FOSS list (incomplete)
	- My favs
		- 5 of my favorites:
		- Blanket
		- Tootle
		- Apostrophe
		- gThumb
		- Fractal

## Proprietary List

I use a couple of proprietary softwares. They are as follows:

- Basically everything on my phone (no ROMS for my phone!)
- Zoom (log on to classes)
- Schoology (get assignments)
- Pear Deck (for classes)
- Google (docs, pear deck, schoology...)

As you can see most of my proprietary software is for school. 

### But, why? Why use these?
I am forced to use these: I cannot do school without them. I wish I could, but I cannot. That is all about why.

## FOSS list

There is:

- GNOME Shell
- gedit
- GNOME Web
- Apostrophe
- Nostalgia
- GNOME Taquin
- GNOME Tali
- Metadata Cleaner
- Netsurf
- Boxes
- Connections
- Firefox
- Polari
- Fractal
- Geary
- gThumb

And it goes on. I will only focus on my favorites for now: here they are.

## My favs

These are basically applications that are a pleasure to use for me. I will list them.

### The first 5...?

You can check them out right [here](https://write.as/zxo/my-first-blog-post/) (thank you @zxo@mastodon.online).

### Blanket
Ever had trouble focusing? Too many people talking, a fight happening, or an airplane circling over your house? Blanket is for you. You can listen to sounds and configure their volumes both in app and through Pipe/Pavu, and get _much_ more work done!

### Tootle
Tootle is a quick Mastodon client for the GNOME desktop. I use it to post from my Mastodon @foreverxml@mastodon.online and it is really nice. Integrates well, and you can open in the browser if you ever need. It also uses `libadwaita` to change its UI for screen sizes!

### Apostrophe
Did you know this post was written in Apostrophe? I actually write **_all_** of my posts in Apostrophe! Apostrophe is a small markdown editor that allows you to write freely, and see a preview _while you write_! Amazing.

### gThumb

gThumb is an advanced image viewer, like Shotwell. But it also plays music and videos. You can see advanced properties, and scrub them with Metadata Cleaner if you want. There is also an advanced editor. gThumb is really for the pros who love GNOME!

### Fractal
Last, but far from least. Fractal is a Matrix client for GNOME that is like Discord, but better. It is being rewritten in GTK/Gdk 4 and will support e2e encryption and be split into group messaging and personal messaging. Hooray for Matrix!

### Where can I find these?

On Flathub or GNOME Software. 

## And...
For now, that's all. Enjoy this bear picture.

![two bears](https://placebear.com/200/300)

