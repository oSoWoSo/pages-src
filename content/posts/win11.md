---
title: "Windows 11 is still garbage, but not landfill garbage like Windows 10"
date: 2021-06-24T21:41:28-06:00
tags:
  - windows
  - development
---
## The Good
You read the title. Yes, Windows 11 still brings many improvements. According to _[Ars Technica](https://arstechnica.com/gadgets/2021/06/microsoft-details-windows-11-with-new-ui-and-android-app-support/)_,
> - _Faster wake from sleep_
> - _Faster Windows Hello biometric authentication_
> - _Faster browsing on Microsoft Edge_
> - _"Browsing on any browser is faster, actually, which is awesome"_
> - _Windows Updates are 40 percent smaller, more efficient, and "happen in the background"_
> - _Decreased energy use, for longer battery life, which Panay describes as making Windows 11 "more responsible"_
> -  _"Maybe most important, Windows 11 is the most secure Windows yet"_

Those are not changes to ignore! But there are many, *many*, ***many*** ignored things. 
## The bad
For example, the [system requirements](https://www.microsoft.com/en-us/windows/windows-11-specifications). 
> - **Processor**: 1 gigahertz (GHz) or faster with 2 or more cores on a compatible 64-bit processor or System on a Chip (SoC)
> - **RAM**: 4 gigabyte (GB)
> - **Storage**: 64 GB or larger storage device _Note: See below under “More information on storage space to keep Windows 11 up-to-date” for more details._ [more is needed to update Windows]
> - **System firmware**: UEFI, Secure Boot capable
> - **TPM**: Trusted Platform Module (TPM) version 2.0
> - **Graphics card**: Compatible with DirectX 12 or later with WDDM 2.0 driver
> - **Display** : High definition (720p) display that is greater than 9” diagonally, 8 bits per color channel
> - **Internet connection and Microsoft accounts**: Windows 11 Home edition requires internet connectivity and a Microsoft account to complete device setup on first use.
> - _Switching a device out of Windows 11 Home in S mode also requires internet connectivity. Learn more about S mode here._
> - For all Windows 11 editions, internet access is required to perform updates and to download and take advantage of some features. A Microsoft account is required for some features.

### Whaaat? Explain.

Let's look at this closer.
> - **RAM**: 4 gigabyte (GB)

Hoooold on. 4 gigabytes? My USD$1k dollar computer has 8 GB! That means it will run like Windows 10 on 4GB. Euuugh, really bad bad bad BAD.
> - **Storage**: 64 GB or larger storage device _Note: See below under “More information on storage space to keep Windows 11 up-to-date” for more details._ [more is needed to update Windows]

Wait! I have a frind with a 128GB SSD. And I have 256 GB + dual boot. Fedora takes up 15-20 GB! Please be smaller!
> - **TPM**: Trusted Platform Module (TPM) version 2.0

What in the heck is a TPM? From the MS docs linked on the original website, it says:
> Trusted Platform Module (TPM) technology is designed to provide hardware-based, security-related functions.

>  Some of the key advantages of using TPM technology are that you can:

> - Generate, store, and limit the use of cryptographic keys.

Great! some more "encryption" that's surveillance, required. Remind me to NEVER install this.
> - **Graphics card**: Compatible with DirectX 12 or later with WDDM 2.0 driver

My last computer had DirectX 11. That was three years ago. Need I say more?
> - **Display** : High definition (720p) display that is greater than 9” diagonally, 8 bits per color channel

Why do we need diagonal size limitation? Oh right, crappy fractional scaling.
And, worst of all:
> - **Internet connection and Microsoft accounts**: Windows 11 Home edition requires internet connectivity and a Microsoft account to complete device setup on first use.
> - _Switching a device out of Windows 11 Home in S mode also requires internet connectivity. Learn more about S mode here._
> - For all Windows 11 editions, internet access is required to perform updates and to download and take advantage of some features. A Microsoft account is required for some features.

Okay. So a FRICKING ONLINE ACCOUNT is REQUIRED to use this crap now? I would rather use a phone indefinitely. And, switching out of Safe Mode requires internet too. Nice, to report to Microsoft that you live in Tucson, Arizona, USA. And, let's not forget updates. I will just make a new section for that.

### Updates for this god-forsaken Windows
Updates take a bazillion hours and 54783 GB of storage space. Seen memes like the one below? ![Me: I will restart my computer rq. Windows Update: you just activated my trap card!](https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fpics.me.me%2Fme-lm-just-gonna-restart-my-computer-real-quick-windows-32054712.png&f=1&nofb=1)

Yeah! This is what I mean. Windows Updates will suck.
## So, continued...
There's also the proprietary problem. There is still spyware and tracking and ads and all that.
## In summary.
I am looking forward to Debian 11 Bullseye much more than Windows 11. It will be hoards better. And GNOME 41 on Sep 22 this year!

That's all. See you soon :)