---
title: "Windows 11, continued"
date: 2021-06-25T14:28:59-06:00
tags:
  - windows
---
It is a day later and some more news. According to [ads and autoplay windows fan club site](https://www.windowscentral.com/one-thing-microsoft-didnt-discuss-windows-11-privacy?utm_source=feedburner&utm_medium=feed&utm_campaign=Feed%3A+wmexperts+%28Windows+Central%29), Windows 11 comes with the latest + greatest spyware. And people on the bird site are worried about it too. See below image:
![Windows 11 = Linux - Privacy](https://www.windowscentral.com/sites/wpcentral.com/files/styles/xlarge/public/field/image/2021/06/windows-11-privacy-tweets.png)
And this is in ADDITION to everything before! Addition! If anything Windows 11 is **worse** for privacy. And that's all.