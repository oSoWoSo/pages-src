---
title: "Windows 11, continued even more"
date: 2021-06-26T08:47:59-06:00
tags:
  - windows
---
As I was waking up today I watched a video on Windows 11 by Mental Outlaw (see below).

{{< invidious XlDBoiQXKSE >}}

So, apparently (according to the [ad windows club](https://www.windowscentral.com/starting-2023-windows-11-will-require-laptops-have-cameras) site) Windows will _require webcams in 2023 on laptops, **as well** as Bluetooth_. Webcams are the **FIRST** thing hacked on prop[rietary] OS's like Windows. And people don't want people seeing them on their webcam. Heck, even _Zuck_ uses webcam tape! And Bluetooth is very insecure. Just search _[bluetooth speaker hacked](https://duckduckgo.com/?q=bluetooth+speaker+hacked)_ and you will know what I mean. And when fixing a Wi-Fi card, it's easy to disconnect Bluetooth.
Now, licensing. Windows 10 did not allow you to customize in unlicensed Windows 10. It did not affect people's productivity. But in Windows 11, it does because of the center taskbar. And ARM support? Oh, oh oh oh oh OH. You know the M1-Linux fiasco? You know Microsoft will do it with Windows! And some more DRM... It is a bit better with VMs though! But, there's more... Watch this (it's a bit old).

{{< invidious JaORmA0E42A >}}

Same old spyware selector in setup. I saw that my mom actually turned ON some of the switches when setting up hew new windows computer? Anyway, back to the point. There is a spyful news feed in it, and lots of unmodified stuff in it from versions past. Anyway, that's all from the person who will stay on Windows 10 for Windows-essential functions. Have a great day, and read my previous two posts!