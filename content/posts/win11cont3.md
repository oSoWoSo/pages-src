---
title: "Microsoft done hecked up with Windows 11"
date: 2021-06-28T12:58:37-06:00
tags:
  - windows
  - rant
---
Windows 11 is supposed to be the "latest and greatest", but it really is _NOT_! Just read my posts tagged #windows. But I want to rant about one thing ONLY: the processor requirements. Windows 11 requires a 8th gen or later Intel processor, or Ryzen 2000 or later. Now, what does this mean? *8th gen Intels were from 2017, and Ryzen 2000 from 2019*. Your computer has to be less than 4 years old to use Windows 11! And some 2 year old computers have Intel 7th gen processors (Mine is 2yo @ the time with an 8th gen.) What the heck, Microsoft? Half, if not MORE than half, of ALL Windows 10 users will be left out in the cold with insecure OS' if you do this, Microsoft. There are people with *seven* PCs with none that can use Windows 11. People will start to switch to the superior OS group, Linux (Linux Mint especially) and you won't be abe to do _anything_ about it. And the KDE Plasma desktop can look similar to yours and do more! And this will create LOTS of e-waste too! You better think this through. You BETTER HECKING think this through. People are pissed. Their computers won't work anymore. And if I used Windows, I would be pissed because MY computer doesn't really work either! Yes, it technically works, but it would be too slow anyway. This is the year of Linux. And it won't change again as you obsolescence people's computers for money, Microsoft.

And thank you for reading this post. If you haven't already, switch to Linux Mint or something like it. People should switch but not too many, as then Linux will not need antiviruses. See you soon.
