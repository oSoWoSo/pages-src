---
title: "Why I'm saying NO to Windows 11"
date: 2021-06-28T10:03:36-06:00
tags:
  - windows
  - uninstall
  - development
---
I will not be installing Windows 11 when it comes around. Yes, my computer _supports_ Windows 11. I screenshotted the PC Health app:  

![My computer supports Windows 11](https://pixey.org/storage/m/_v2/314170033553149952/e5f564f55-b772c0/x0IjYFuUvq9p/XBIzSQQrQ0cmCmIM9rP1isf48297xj2fz8skQOub.png)

So, yes my computer *CAN* run 11, but do I want to? No. Remember my [post about Windows 11 system requirements](https://foreverxml.codeberg.page/posts/win11/)? I forgot to mention something. _Intel 7th gen and lower chips are **NOT** supported in Windows 11_. Now, bad news: My chip is 8th gen. And I have 8 GB of RAM. Remember what Windows 10 runs like on 4GB of RAM? Windows 11 on 8GB will run like that too. I had a computer that met minimum processor and double minimum RAM on Windows 10. It was *so frickin SLOW* that I moved it to Fedora. That will happen with 8GB RAM computers. And Windows 11 on the Ads, DRM, and Spyware scale from 0-10 scores an 11 in every category (IMO). And, you can make KDE Plasma look _just like_ Windows 11. No kidding, someone actually did it! See the image below:

![ let win11 = kde-plasma; ](https://libredd.it/img/x3kxew2luz771.jpg)

So, hehe, Windows 11 is KDE. And KDE is ligher, and FOSS. So goodbye Windows, see you never.