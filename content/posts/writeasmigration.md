---
title: "Why my «first post» isn't actually first"
date: 2021-06-24T14:56:28-06:00
tags:
  - blog
  - browser
  - development
---
Once upon a time, I wanted a fast and easy blog. I was looking through my mastodon, and found [Write.as](https://write.as). I set up an account and wrote some posts! Fast forward to now, and I have a Hugo. I remember the Write.as, so I get the posts over, add proper date marks, and incinerate the account. And these posts were written before my first post (the _index_ **markdown** thingy?). And thus, my first post is not actually first.

> Migrating can be hard.
> 
> >_Sharky, a blåhaj from IKEA_

This is the end of this post. No sharky pictures right now :(